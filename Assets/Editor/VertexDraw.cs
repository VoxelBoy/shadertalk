﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class VertexDraw : EditorWindow
{

	int vertexIndex = 128;
	Mesh mesh;
	Transform t;
	Vector3 vertex;

	bool draw = true;

	GUIStyle labelStyle;

	enum Space
	{
		ObjectSpace,
		WorldSpace,
		ViewSpace,
		ScreenSpace}

	;

	Space space = Space.ObjectSpace;
	Space lastSpace = Space.ObjectSpace;

	[MenuItem ("Window/VertexDraw")]
	public static void ShowVertexDrawWindow ()
	{
		EditorWindow.GetWindow<VertexDraw> ();
	}

	void OnEnable ()
	{
		SceneView.onSceneGUIDelegate += HandleOnSceneFunc;
	}

	void OnDisable ()
	{
		SceneView.onSceneGUIDelegate -= HandleOnSceneFunc;
	}

	void HandleOnSceneFunc (SceneView sceneView)
	{
		if (!draw || t == null || mesh == null) {
			return;
		}

		if (space != lastSpace) {
			if (space == Space.ObjectSpace) {
				sceneView.LookAt (t.position + t.forward + Vector3.up, t.rotation * Quaternion.AngleAxis (180, Vector3.up), 1f, false, false);
				sceneView.renderMode = DrawCameraMode.Wireframe;
			} else if (space == Space.WorldSpace) {
				sceneView.LookAt (new Vector3 (0f, 3.6f, 6.5f), Quaternion.Euler (19.25f, 148.3f, 0f), 0f, false, false);
				sceneView.renderMode = DrawCameraMode.Wireframe;
			} else if (space == Space.ViewSpace) {
				sceneView.LookAt (Camera.main.transform.position, Camera.main.transform.rotation, 0f, false, false);
				sceneView.renderMode = DrawCameraMode.Textured;
			} else if (space == Space.ScreenSpace) {
				sceneView.LookAt (Camera.main.transform.position, Camera.main.transform.rotation, 0f, false, false);
				sceneView.renderMode = DrawCameraMode.Textured;
			}

			lastSpace = space;
		}

		Vector3 vertexWorld = t.TransformPoint (vertex);
		Handles.DotCap (-1, vertexWorld, Quaternion.identity, 0.015f);

		Vector3 vertexScreen = sceneView.camera.WorldToScreenPoint (vertexWorld);
		Rect screenRect = new Rect (vertexScreen.x + 10, sceneView.position.height - vertexScreen.y - 25, 200, 40);

		GUILayout.BeginArea (screenRect);

		string vertexString = GetVertexString (vertex, t, sceneView.camera);

		labelStyle = new GUIStyle (EditorStyles.largeLabel);
		labelStyle.fontSize = 20;
		labelStyle.fontStyle = FontStyle.Bold;

		GUI.color = Color.white;
		GUILayout.Label (vertexString, labelStyle);
		var lastRect = GUILayoutUtility.GetLastRect ();
		lastRect.x -= 1f;
		lastRect.y -= 1f;
		GUI.color = Color.black;
		GUI.Label (lastRect, vertexString, labelStyle);
		GUILayout.EndArea ();

//		GUILayout.BeginArea (new Rect (0, 0, 200, 30));
//		GUILayout.Label (Vec3FullString (sceneView.camera.transform.position));
//		GUILayout.EndArea ();
	}

	string GetVertexString (Vector3 vert, Transform _t, Camera c)
	{
		if (space == Space.ObjectSpace) {
			return vert.ToString ();
		} else if (space == Space.WorldSpace) {
			return _t.TransformPoint (vert).ToString ();
		} else if (space == Space.ViewSpace) {
			var vertexWorld = _t.TransformPoint (vert);
			return c.WorldToViewportPoint (vertexWorld).ToString ();
		} else if (space == Space.ScreenSpace) {
			var vertexWorld = _t.TransformPoint (vert);
			return c.WorldToScreenPoint (vertexWorld).ToString ();
		}
		return null;
	}

	void OnSelectionChange ()
	{
		t = Selection.activeTransform;
		if (t != null) {
			var skinnedRenderer = t.GetComponentInChildren<SkinnedMeshRenderer> ();
			if (skinnedRenderer != null) {
				mesh = skinnedRenderer.sharedMesh;
			} else {
				var meshFilter = t.GetComponentInChildren<MeshFilter> ();
				if (meshFilter != null) {
					mesh = meshFilter.sharedMesh;
				}
			}
		}
		this.Repaint ();
	}

	void OnGUI ()
	{
		draw = GUILayout.Toggle (draw, "Draw");
		if (GUI.changed) {
			SceneView.RepaintAll ();
			GUI.changed = false;
		}

		GUI.changed = false;
		vertexIndex = EditorGUILayout.IntField ("Vertex index", vertexIndex);
		if (GUI.changed && t != null && mesh != null) {
			vertex = vertexIndex >= 0 && vertexIndex < mesh.vertices.Length ? mesh.vertices [vertexIndex] : Vector3.zero;
			SceneView.RepaintAll ();
			GUI.changed = false;
		}

		if (t == null || mesh == null) {
			GUILayout.Label ("No mesh selected");
		}

		GUI.changed = false;
		space = (Space)EditorGUILayout.EnumPopup ("Space", space);
		if (GUI.changed) {
			SceneView.RepaintAll ();
			GUI.changed = false;
		}
	}

	string Vec3FullString (Vector3 vec)
	{
		return string.Format ("{0},{1},{2}", vec.x, vec.y, vec.z);
	}
}
